package com.mmrbd.bs23androidtask.feature.repodetailsview

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.SubcomposeAsyncImage
import com.mmrbd.bs23androidtask.data.data_source.remote.model.RepoItem
import com.mmrbd.bs23androidtask.utils.extensions.convertDate

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun RepoDetailsView(
    repoItem: RepoItem
) {
    Column {
        Column(
            modifier = Modifier.padding(10.dp)
        ) {
            SubcomposeAsyncImage(
                model = repoItem.owner.avatar_url,
                loading = {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .padding(10.dp)
                    )
                },
                contentDescription = "",
                modifier = Modifier
                    .size(height = 70.dp, width = 70.dp)
                    .clip(CircleShape)
            )
            Spacer(modifier = Modifier.width(10.dp))
            Text(text = repoItem.name, fontSize = 24.sp, color = Color.Blue)
            Text(text = "Owner: ${repoItem.owner.login}")
            Text(
                text = "${repoItem.description}",
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
            FlowRow(
                modifier = Modifier.padding(vertical = 12.dp)
            ) {
                repoItem.topics.forEach {
                    Card(
                        shape = CardDefaults.outlinedShape,
                        modifier = Modifier
                            .padding(5.dp)
                    ) {
                        Text(
                            text = it, fontSize = 12.sp, modifier = Modifier
                                .padding(8.dp)
                        )
                    }
                }

            }
            Text(text = "Language: ${repoItem.language}", fontWeight = FontWeight.Bold)
            Text(
                text = "Last Updated: ${repoItem.updated_at.convertDate()}",
                fontWeight = FontWeight.Bold
            )
        }
    }
}