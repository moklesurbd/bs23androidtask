package com.mmrbd.bs23androidtask.feature.repoview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mmrbd.bs23androidtask.base.api.ApiResponse
import com.mmrbd.bs23androidtask.data.data_source.remote.model.RepoItem
import com.mmrbd.bs23androidtask.domain.Repository.GithubRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GithubRepositoryViewModel @Inject constructor(
    private val repository: GithubRepository
) : ViewModel() {

    private val _usersStateFlow =
        MutableStateFlow<ApiResponse<List<RepoItem>>>(ApiResponse.Loading())
    val usersStateFlow: StateFlow<ApiResponse<List<RepoItem>>> = _usersStateFlow

    init {
        getGithubUser("Android", "stars")
    }

    private fun getGithubUser(query: String, sort: String) = viewModelScope
        .launch {
            repository.getGithubRepo(query, sort)
                .onStart {
                    _usersStateFlow.emit(ApiResponse.Loading())
                }
                .collect {
                    _usersStateFlow.emit(it)
                }
        }
}