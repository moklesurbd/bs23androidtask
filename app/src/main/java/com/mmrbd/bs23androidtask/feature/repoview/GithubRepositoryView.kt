package com.mmrbd.bs23androidtask.feature.repoview

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.SubcomposeAsyncImage
import com.mmrbd.bs23androidtask.base.api.ApiResponse
import com.mmrbd.bs23androidtask.base.failure.NetworkFailureMessage
import com.mmrbd.bs23androidtask.data.data_source.remote.model.RepoItem
import com.mmrbd.bs23androidtask.ui.nav.Screen
import com.mmrbd.bs23androidtask.utils.AppLogger
import com.mmrbd.bs23androidtask.utils.extensions.convertDate

@Composable
fun GithubRepositoryView(
    navController: NavController,
    viewModel: GithubRepositoryViewModel,
    networkFailureMessage: NetworkFailureMessage,
) {
    val usesState by viewModel.usersStateFlow.collectAsState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(18.dp)
    ) {
        when (usesState) {
            is ApiResponse.Loading -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    CircularProgressIndicator()
                    AppLogger.log(usesState.data.toString())
                }
            }

            is ApiResponse.Error -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    Text(text = networkFailureMessage.handleFailure(usesState.error!!))
                }
            }

            is ApiResponse.Success -> {
                LazyColumn {
                    usesState.data?.let { data ->
                        items(data.size, key = { data[it].id }) { index ->
                            RepoItem(data[index]) {
                                navController.currentBackStackEntry?.savedStateHandle?.set(
                                    key = "repoItem",
                                    value = it
                                )
                                navController.navigate(
                                    Screen.RepoDetails.route + "?repoItem$it}"
                                )
                            }
                        }
                    }
                }
            }
        }

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RepoItem(
    repoItem: RepoItem,
    onClickItem: (RepoItem) -> Unit
) {

    OutlinedCard(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 10.dp),
        onClick = { onClickItem(repoItem) }
    ) {
        Row(
            modifier = Modifier.padding(10.dp)
        ) {
            SubcomposeAsyncImage(
                model = repoItem.owner.avatar_url,
                loading = {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .padding(10.dp)
                    )
                },
                contentDescription = "",
                modifier = Modifier
                    .size(height = 70.dp, width = 70.dp)
                    .clip(CircleShape)
            )
            Spacer(modifier = Modifier.width(10.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = repoItem.name, fontSize = 24.sp, color = Color.Blue)
                Text(text = "Owner: ${repoItem.owner.login}")
                Text(
                    text = "${repoItem.description}",
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis
                )
                Text(text = "Language: ${repoItem.language}", fontWeight = FontWeight.Bold)
                Text(
                    text = "Last Updated: ${repoItem.updated_at.convertDate()}",
                    fontWeight = FontWeight.Bold
                )
            }
        }
    }
}
