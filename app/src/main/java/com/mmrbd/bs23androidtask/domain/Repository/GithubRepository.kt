package com.mmrbd.bs23androidtask.domain.Repository

import com.mmrbd.bs23androidtask.base.api.ApiResponse
import com.mmrbd.bs23androidtask.data.data_source.remote.model.RepoItem
import kotlinx.coroutines.flow.Flow

interface GithubRepository {
    suspend fun getGithubRepo(query: String, sort: String): Flow<ApiResponse<List<RepoItem>>>
}