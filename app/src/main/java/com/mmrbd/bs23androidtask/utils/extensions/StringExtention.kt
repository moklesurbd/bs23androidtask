package com.mmrbd.bs23androidtask.utils.extensions

import android.annotation.SuppressLint
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.Date

@SuppressLint("SimpleDateFormat")
fun String.convertDate(): String? {
    val instant: Instant = Instant.parse(this)
    val myDate = Date.from(instant)
    val formatter = SimpleDateFormat("MM-dd-yy HH:ss")
    return formatter.format(myDate)
}

fun <T> String.fromJson(type: Class<T>): T {
    return Gson().fromJson(this, type)
}

fun <T> T.toJson(): String? {
    return Gson().toJson(this)
}