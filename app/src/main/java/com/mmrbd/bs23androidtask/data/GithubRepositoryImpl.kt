package com.mmrbd.bs23androidtask.data

import com.mmrbd.bs23androidtask.base.api.ApiResponse
import com.mmrbd.bs23androidtask.base.failure.Failure
import com.mmrbd.bs23androidtask.base.failure.getErrorTypeByHTTPCode
import com.mmrbd.bs23androidtask.data.data_source.remote.GithubApiService
import com.mmrbd.bs23androidtask.data.data_source.remote.model.RepoItem
import com.mmrbd.bs23androidtask.domain.Repository.GithubRepository
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.net.UnknownHostException
import javax.inject.Inject

class GithubRepositoryImpl @Inject constructor(
    private val apiService: GithubApiService
) : GithubRepository {
    override suspend fun getGithubRepo(query: String, sort: String): Flow<ApiResponse<List<RepoItem>>> =
        callbackFlow {

            trySend(ApiResponse.Loading())
            try {

                val response = apiService.getGithubUsers(query, sort)
                if (response.isSuccessful) {
                    trySend(ApiResponse.Success(response.body()!!.items))
                } else {
                    trySend(
                        ApiResponse.Error(
                            getErrorTypeByHTTPCode(response.code())
                        )
                    )
                }

            } catch (exception: Exception) {
                when (exception) {
                    is UnknownHostException -> {
                        trySend(ApiResponse.Error(Failure.HTTP.NetworkConnection))
                    }

                    else -> {
                        trySend(ApiResponse.Error(Failure.Exception(exception)))
                    }
                }
            }

            awaitClose()
        }
}