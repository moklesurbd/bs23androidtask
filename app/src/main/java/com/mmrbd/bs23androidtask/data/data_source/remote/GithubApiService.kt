package com.mmrbd.bs23androidtask.data.data_source.remote

import com.mmrbd.bs23androidtask.base.api.ApiEndPoint
import com.mmrbd.bs23androidtask.data.data_source.remote.model.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApiService {

    @GET(ApiEndPoint.SearchRepo)
    suspend fun getGithubUsers(@Query("q") query: String, @Query("sort") sort: String): Response<SearchResponse>
}