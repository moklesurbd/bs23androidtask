package com.mmrbd.bs23androidtask.base.di

import com.mmrbd.bs23androidtask.data.GithubRepositoryImpl
import com.mmrbd.bs23androidtask.data.data_source.remote.GithubApiService
import com.mmrbd.bs23androidtask.domain.Repository.GithubRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object ApiModule {

    @Provides
    fun getGithubRepo(apiService: GithubApiService): GithubRepository =
        GithubRepositoryImpl(apiService)

}