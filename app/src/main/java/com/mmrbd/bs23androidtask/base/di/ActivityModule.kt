package com.mmrbd.bs23androidtask.base.di

import android.content.Context
import com.mmrbd.bs23androidtask.base.failure.NetworkFailureMessage
import com.mmrbd.bs23androidtask.base.failure.NetworkFailureMessageImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {

    @Provides
    fun getNetworkFailureMessage(@ApplicationContext context: Context): NetworkFailureMessage =
        NetworkFailureMessageImpl(context)
}