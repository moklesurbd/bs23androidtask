package com.mmrbd.bs23androidtask.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BS23AndroidTaskApp : Application()