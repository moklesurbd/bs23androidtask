package com.mmrbd.bs23androidtask.base.api

import com.mmrbd.bs23androidtask.base.failure.Failure


/**
 * A wrapper for network request handling failing requests
 * @param data
 *
 * */
sealed class ApiResponse<T>(
    val data: T? = null,
    val error: Failure? = null,
) {
    class Success<T>(data: T) : ApiResponse<T>(data)
    class Loading<T>(data: T? = null) : ApiResponse<T>(data)
    class Error<T>(throwable: Failure, data: T? = null) : ApiResponse<T>(data, throwable)

}
