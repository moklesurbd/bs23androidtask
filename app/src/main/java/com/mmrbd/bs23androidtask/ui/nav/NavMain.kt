package com.mmrbd.bs23androidtask.ui.nav

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.mmrbd.bs23androidtask.base.failure.NetworkFailureMessage
import com.mmrbd.bs23androidtask.data.data_source.remote.model.RepoItem
import com.mmrbd.bs23androidtask.feature.repodetailsview.RepoDetailsView
import com.mmrbd.bs23androidtask.feature.repoview.GithubRepositoryView
import com.mmrbd.bs23androidtask.feature.repoview.GithubRepositoryViewModel
import com.mmrbd.bs23androidtask.utils.AppLogger

sealed class Screen(val route: String) {
    object GithubRepository : Screen("repo_view")
    object RepoDetails : Screen("details_view")
}


@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun NavHostMain(
    navController: NavHostController,
    networkFailureMessage: NetworkFailureMessage
) {


    NavHost(navController = navController, startDestination = Screen.GithubRepository.route) {

        composable(Screen.GithubRepository.route) {
            val viewModel = hiltViewModel<GithubRepositoryViewModel>()

            GithubRepositoryView(
                navController,
                viewModel,
                networkFailureMessage
            )
        }

        composable(
            Screen.RepoDetails.route
        ) {
            val repoItem =
                navController.previousBackStackEntry?.savedStateHandle?.get<RepoItem>("repoItem")

            AppLogger.log("DATA $repoItem")

            repoItem?.let {
                RepoDetailsView(repoItem = it)
            }

        }
    }

}