plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")


    kotlin("kapt")
    id("com.google.dagger.hilt.android")
}

android {
    namespace = "com.mmrbd.bs23androidtask"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.mmrbd.bs23androidtask"
        minSdk = 26
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }

        buildConfigField(
            "String",
            "GITHUB_BASE_API_URL",
            project.properties["GITHUB_BASE_API_URL"].toString()
        )
    }

    flavorDimensions += "version"
    productFlavors {
        create("dev") {
            dimension = "version"

        }

        create("qa") {
            dimension = "version"
        }

        create("staging") {
            dimension = "version"
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"

    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation(Dependencies.AndroidX.coreKtx)
    implementation(Dependencies.AndroidX.lifecycle)
    implementation(Dependencies.Compose.activityCompose)
    implementation(platform(Dependencies.Compose.composeBom))

    implementation(Dependencies.Compose.composeUi)
    implementation(Dependencies.Compose.composeGraphics)
    implementation(Dependencies.Compose.composeNavigation)

    implementation(Dependencies.Hilt.hiltNav)

    // Retrofit
    implementation(Dependencies.Retrofit.retrofit)
    implementation(Dependencies.Retrofit.retrofitConverter)

    // Okhttp
    implementation(Dependencies.Okhttp.okHttp)
    implementation(Dependencies.Okhttp.okHttpLogging)

    // Hilt
    implementation(Dependencies.Hilt.hilt)
    kapt(Dependencies.Hilt.hiltKapt)

    implementation(Dependencies.Coin.coin)

    debugImplementation(Dependencies.Compose.composeTooling)
    debugImplementation(Dependencies.Compose.manifest)

    implementation(Dependencies.Compose.composePreview)
    implementation(Dependencies.Compose.composeMaterial3)

    testImplementation(Dependencies.Compose.androidTestJunitEtx)

    androidTestImplementation(Dependencies.Compose.androidTestJunitEtx)
    androidTestImplementation(Dependencies.Compose.androidTestEspresso)
    androidTestImplementation(platform(Dependencies.Compose.composeBom))
    androidTestImplementation(Dependencies.Compose.androidTestJunit4)
}