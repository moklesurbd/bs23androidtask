# Star War Explorer
This is demo project, Using android modern development technology for scalable, maintainable and testable code.

## Build and Run
- Clone the Repository. 
```
$ git clone https://gitlab.com/moklesurbd/bs23androidtask.git
```
- Build and Run the Project.


## Stack Used
- MVVM
- Clean Architecture
- Dependency Injection (Hilt)
- REST

## Dependencies
- Jetpack Compose
- Android Core (KTX)
- Android Lifecycle
- Navigation Component
- Retrofit
- OkHttp
- Gson
- Hilt

Screenshot: 
![Repo List](repo_list.png) ![Repo Details](repo_details.png)

